module.exports = {
  primary: {
    lighter: { "value": "{color.core.blue.60.value}"  },
    light:   { "value": "{color.core.blue.80.value}"  },
    base:    { "value": "{color.core.blue.100.value}" },
    dark:    { "value": "{color.core.blue.120.value}" },
    darker:  { "value": "{color.core.blue.140.value}" }
  },
  secondary: {
    lighter: { "value": "{color.core.green.60.value}"  },
    light:   { "value": "{color.core.green.80.value}"  },
    base:    { "value": "{color.core.green.100.value}" },
    dark:    { "value": "{color.core.green.120.value}" },
    darker:  { "value": "{color.core.green.140.value}" }
  },
  warning: {
    light:   { "value": "{color.core.red.80.value}"  },
    base:    { "value": "{color.core.red.100.value}" },
    dark:    { "value": "{color.core.red.120.value}" },
  }

}