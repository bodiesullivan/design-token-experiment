const buildPath = 'dist/';
const baseTokens = require('design-token-experiment/tokens');

module.exports = {
  tokens: baseTokens,
  source: [
    `tokens/index.js`
  ],
  platforms: {
    css: {
      transformGroup: 'css',
      buildPath: buildPath,
      files: [{
        destination: 'variables.css',
        format: 'css/variables'
      }]
    },
  }
}