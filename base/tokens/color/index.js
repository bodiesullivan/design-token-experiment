module.exports = {
  core: require('./core'),
  brand: require('./brand'),
  font: require('./font'),
  background: require('./background'),
  border: require('./border')
}