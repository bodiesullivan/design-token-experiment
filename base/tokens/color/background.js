module.exports = {
  base: { value: "{color.core.white.100.value}" },
  alt: { value: "{color.core.grey.20.value}" },
  link: { value: "{color.brand.primary.base.value}" }
};