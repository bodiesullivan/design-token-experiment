module.exports = {
  color: require('./color'),
  size: require('./size'),
  components: require('./components'),
}