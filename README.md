# Design Token Experiment
This is an experiment to prove an approach of creating a NPM package of base design tokens that can be consumed and extended by another project.

Example use case:
A company with multiple products, brands, & platforms wants to offer a standard design style and components that can be consumed and overridden by each consuming product/brand/platform.

## Projects in this repository
- base - The project that represents the parent design system and contains the base design tokens. 
- consumer - The project representing a single product that wants to consume and extend the parent design systems tokens.

# Setting up the experiment

## Clone the repo

## Setup Base Project
1. Navigate to `/base` in the console.
2. Run `npm install` to install all dependencies.
3. Run `npm run build` to build the base design tokens and css.
    - This step is optional as the consumer doesn't use the transformed css. However this does represent the normal output of a design system's token library.
4. Run `npm link` to create add the base project to your local global npm cache.

## Setup Consumer project
1. Navigate to `/consumer` in the console.
2. Run `npm install` to install all dependencies.
3. Run `npm link design-token-experiment` to link the base project as an npm dependency.
4. Run `npm run build` to build the consumer projects design tokens.

# Things to check out
- The tokens are all written as CommonJS modules rather than JSON.
  - This makes it easier to write code against them, import the results, and improves the develeloper experience while working with them.
- The consumer `config.js` imports the base token collection as a plain javascript object and injects them into the style-dictionary config as `tokens`.
- Compare the token overrides and additions that the consumer project makes:
  - brand.js - primary & secondary colors switched from teal & blue to blue & green.
  - brand.js - warning variant is added.
